#!/bin/sh

DIR=$(dirname "${0}")

# For ZooKeeper properties, see
# https://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_configuration

# variable names and the format of their values is according to https://github.com/31z4/zookeeper-docker

if [ -n "${PROP_ZK_clientPort}" ]; then
	export ZOO_PORT="${PROP_ZK_clientPort}"
else
	export ZOO_PORT="${ZOO_PORT:-2181}"
	export PROP_ZK_clientPort="${ZOO_PORT}"
fi

if [ -n "${PROP_ZK_dataDir}" ]; then
	export ZOO_DATA_DIR="${PROP_ZK_dataDir}"
fi
if [ -n "${ZOO_DATA_DIR}" ]; then
	mkdir -vp "${ZOO_DATA_DIR}"
	chmod -v 700 "${ZOO_DATA_DIR}"
	if [ ! -f "${ZOO_DATA_DIR}/myid" ]; then
		echo "${ZOO_MY_ID:-1}" > "${ZOO_DATA_DIR}/myid"
	fi
	chown -R kafka:kafka "${ZOO_DATA_DIR}"
	export PROP_ZK_dataDir="${ZOO_DATA_DIR}"
fi

if [ -n "${PROP_ZK_dataLogDir}" ]; then
	export ZOO_DATA_LOG_DIR="${PROP_ZK_dataLogDir}"
fi
if [ -n "${ZOO_DATA_LOG_DIR}" ]; then
	mkdir -vp "${ZOO_DATA_LOG_DIR}"
	chmod -v 700 "${ZOO_DATA_LOG_DIR}"
	chown -R kafka:kafka "${ZOO_DATA_LOG_DIR}"
	export PROP_ZK_dataLogDir="${ZOO_DATA_LOG_DIR}"
fi

if [ -z "${ZOO_SERVERS}" ]; then
	export PROP_ZK_server_1="localhost:2888:3888"
	# for ZK v3.5 and later the client port is set here for individual servers, not above in the clientPort property
	#export PROP_ZK_server_1="localhost:2888:3888;${ZOO_PORT}"
else
	for ZOO_SERVER in ${ZOO_SERVERS}; do
		eval "export PROP_ZK_${ZOO_SERVER%%.*}_${ZOO_SERVER#*.}"
	done
fi

. "${DIR}/kafka-set-props.sh"

exec su kafka -c "exec ${KAFKA_HOME}/bin/zookeeper-server-start.sh ${ZOOKEEPER_CONF} ${@}"
