#!/bin/sh

case "${ROLE}" in
	""|kafka)
		exec su kafka -c '${KAFKA_HOME}/bin/kafka-broker-api-versions.sh --bootstrap-server "${LISTENERS}" || exit 1'
		;;
	zookeeper)
		exec su kafka -c '${KAFKA_HOME}/bin/zookeeper-shell.sh localhost:${ZOO_PORT} stat / || exit 1'
		;;
	*)
		echo "Unknow role '${ROLE}', cannot perform the health-check." >&2
		exit 0
esac
