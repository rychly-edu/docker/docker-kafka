# https://hub.docker.com/r/adoptopenjdk/openjdk8
# https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim
FROM adoptopenjdk/openjdk8:alpine-slim

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

# https://kafka.apache.org/downloads
ARG KAFKA_VERSION=1.1.0
ARG SCALA_VERSION=2.12

ARG DOWNLOAD_CACHE
#ARG APACHE_ORIG="http://www-eu.apache.org/dist"	# mirror disabled as it does not have the old versions, just the latest
ARG APACHE_ORIG="http://archive.apache.org/dist"
ARG APACHE_MIRROR="https://archive.apache.org/dist"

ENV KAFKA_HOME="/opt/kafka"
ENV KAFKA_CONF_DIR="${KAFKA_HOME}/config"
ENV \
KAFKA_SERVER_CONF="${KAFKA_CONF_DIR}/server.properties" \
ZOOKEEPER_CONF="${KAFKA_CONF_DIR}/zookeeper.properties"

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
# bash: kafka shell scripts require BASH, they are not compatible with Busybox ASH/SH
# sed: scripts to set Kafka properties in files require GNU sed (the usage of busybox sed may result into incorrect outputs)
&& apk add --no-cache --update gnupg attr sed bash \
\
# download keys and trust them
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/kafka.KEYS" /tmp \
	|| wget -O /tmp/kafka.KEYS "${APACHE_ORIG}/kafka/KEYS" ) \
&& gpg --import /tmp/kafka.KEYS \
&& echo "trust-model always" > ~/.gnupg/gpg.conf \
\
# download the package
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz" /tmp \
	|| wget -O /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz "${APACHE_MIRROR}/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz" ) \
\
# download and verify signature
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz.asc" /tmp \
	|| wget -O /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz.asc "${APACHE_ORIG}/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz.asc" ) \
&& for SIG in /tmp/*.asc; do gpg --verify "${SIG}" "${SIG%.asc}"; done \
\
# extract the package and remove garbage
&& mkdir -p "${KAFKA_HOME}" \
&& tar -xzf "/tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz" -C "${KAFKA_HOME}" --strip-components 1 \
&& find "${KAFKA_HOME}" \( -name '*.asc' -o -name '*-sources.jar' -o -name '*-javadoc.jar' -o -name '*-scaladoc.jar' -o -name '*-test.jar' -o -name '*-examples-*.jar' \) -delete \
&& rm -rf "${KAFKA_HOME}/bin/windows" "${KAFKA_HOME}/site-docs" \
\
# fix ISSUE: os::commit_memory failed; error=Operation not permitted
# (AUFS does not support xattr, so we need to set the flag once again after execution of the container in its entrypoint)
# https://en.wikibooks.org/wiki/Grsecurity/Application-specific_Settings#Java
&& setfattr -n user.pax.flags -v em "${JAVA_HOME}/bin/java" "${JAVA_HOME}/jre/bin/java" \
\
# set up permissions
&& addgroup -S kafka \
&& adduser -h ${KAFKA_HOME} -g "Apache Kafka" -s /bin/sh -G kafka -S -D -H kafka \
&& chown -R kafka:kafka ${KAFKA_HOME} \
\
# set path and java-home for the shell
&& echo '#!/bin/sh' > /etc/profile.d/path-kafka.sh \
&& echo "export PATH=\"\${PATH}:${KAFKA_HOME}/bin\"" >> /etc/profile.d/path-kafka.sh \
&& echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile.d/path-kafka.sh \
&& chmod 755 /etc/profile.d/path-kafka.sh \
\
# clean up
&& apk del gnupg \
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
